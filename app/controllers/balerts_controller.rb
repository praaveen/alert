class BalertsController < ApplicationController
  # GET /balerts
  # GET /balerts.json
  def index
    @balerts = Balert.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @balerts }
    end
  end

  # GET /balerts/1
  # GET /balerts/1.json
  def show
    @balert = Balert.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @balert }
    end
  end

  # GET /balerts/new
  # GET /balerts/new.json
  def new
    @balert = Balert.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @balert }
    end
  end

  # GET /balerts/1/edit
  def edit
    @balert = Balert.find(params[:id])
  end

  # POST /balerts
  # POST /balerts.json
  def create
    @balert = Balert.new(params[:balert])

    respond_to do |format|
      if @balert.save
        format.html { redirect_to @balert, notice: 'Balert was successfully created.' }
        format.json { render json: @balert, status: :created, location: @balert }
      else
        format.html { render action: "new" }
        format.json { render json: @balert.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /balerts/1
  # PUT /balerts/1.json
  def update
    @balert = Balert.find(params[:id])

    respond_to do |format|
      if @balert.update_attributes(params[:balert])
        format.html { redirect_to @balert, notice: 'Balert was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @balert.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /balerts/1
  # DELETE /balerts/1.json
  def destroy
    @balert = Balert.find(params[:id])
    @balert.destroy

    respond_to do |format|
      format.html { redirect_to balerts_url }
      format.json { head :no_content }
    end
  end

  def gen_alert
    #Birthday
    @bdays = Balert.find(:all, :conditions => ["DAY(friend_dob) = ? AND MONTH(friend_dob) = ?", 2.days.from_now.day, 2.days.from_now.month])
    if !@bdays.empty?
       @bdays.each do |alertt| UserMailer.demo_mail(alertt,'Birthday').deliver! end
    end
    # Anniversary
    @anni = Balert.find(:all, :conditions => ["DAY(friend_marriage_anni_date) = ? AND MONTH(friend_marriage_anni_date) = ?", 2.days.from_now.day, 2.days.from_now.month])
    if !@anni.empty?
      @anni.each do |alertt| UserMailer.demo_mail(alertt,'Wedding Anniversary').deliver! end
    end
  end


end
