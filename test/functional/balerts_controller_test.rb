require 'test_helper'

class BalertsControllerTest < ActionController::TestCase
  setup do
    @balert = balerts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:balerts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create balert" do
    assert_difference('Balert.count') do
      post :create, balert: @balert.attributes
    end

    assert_redirected_to balert_path(assigns(:balert))
  end

  test "should show balert" do
    get :show, id: @balert
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @balert
    assert_response :success
  end

  test "should update balert" do
    put :update, id: @balert, balert: @balert.attributes
    assert_redirected_to balert_path(assigns(:balert))
  end

  test "should destroy balert" do
    assert_difference('Balert.count', -1) do
      delete :destroy, id: @balert
    end

    assert_redirected_to balerts_path
  end
end
