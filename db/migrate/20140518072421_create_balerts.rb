class CreateBalerts < ActiveRecord::Migration
  def change
    create_table :balerts do |t|
      t.string :user_name
      t.string :user_email
      t.string :friend_name
      t.string :friend_email
      t.date :friend_dob
      t.date :friend_marriage_anni_date

      t.timestamps
    end
  end
end
